package com.jaguarlabs.baseproject.fragments.pager;

import android.graphics.Color;
import android.support.v4.view.ViewPager;

import com.astuetz.PagerSlidingTabStrip;
import com.jaguarlabs.baseproject.R;
import com.jaguarlabs.baseproject.fragments.bases.BasePagerFragment;
import com.jaguarlabs.baseproject.fragments.bases.BaseSinglePageFragment;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Carlos Briseño on 25/02/2016.
 */
@EFragment(R.layout.fragment_slides)
public class PagerFragment extends BasePagerFragment{

    @ViewById
    PagerSlidingTabStrip tabs;

    @ViewById
    ViewPager vpFragments;

    //region super override methods

    @Override
    public ViewPager getViewPager() {
        return vpFragments;
    }

    @Override
    public PagerSlidingTabStrip getTabs() {
        tabs.setDividerColor(Color.WHITE);
        tabs.setIndicatorColor(Color.WHITE);
        tabs.setTextColor(Color.WHITE);
        tabs.setAllCaps(true);

        return tabs;
    }

    @Override
    public List<BaseSinglePageFragment> getPageFragments() {
        List<BaseSinglePageFragment> fragments = new ArrayList<>();

        //Add your fragment instances to this list

        return fragments;
    }

    //endregion
}
