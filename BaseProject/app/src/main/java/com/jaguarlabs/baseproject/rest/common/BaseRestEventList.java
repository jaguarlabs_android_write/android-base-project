package com.jaguarlabs.baseproject.rest.common;

import java.util.List;

/**
 * Created by Carlos Briseño on 09/05/2016.
 */
public abstract class BaseRestEventList <T> {
    private boolean isSuccessful;
    private List<T> itemList;
    private long requestCode;

    public BaseRestEventList(boolean isSuccessful, List<T> itemList) {
        this.isSuccessful = isSuccessful;
        this.itemList = itemList;
    }

    public long getRequestCode() {
        return requestCode;
    }

    public void setRequestCode(long requestCode) {
        this.requestCode = requestCode;
    }

    public boolean isSuccessful() {
        return isSuccessful;
    }

    public List<T> getItemList() {
        return itemList;
    }
}
