package com.jaguarlabs.baseproject.config;

/**
 * Created by Carlos Briseño on 09/05/2016.
 */
public class Const {

    public static final boolean QUICK_ACCESS = true;
    public static final boolean LOG_GENERAL = true;
    public static final String APP_TAG = "BASE_PROJECT";

    public static final String BASE_URL = "http://192.168.56.1:80/blog/public/";
    public static final String ENDPOINT_USERS = "users";
    public static final String LOGIN_EMAIL = "carlosbrisoft@gmail.com";
    public static final String LOGIN_PASSWORD = "qwerty";

}
