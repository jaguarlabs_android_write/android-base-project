package com.jaguarlabs.baseproject.fragments.drawer;

import android.content.DialogInterface;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;

import com.jaguarlabs.baseproject.R;
import com.jaguarlabs.baseproject.fragments.bases.BaseDrawerFragment;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Carlos Briseño on 25/02/2016.
 */
@EFragment(R.layout.fragment_drawer)
public class DrawerFragment extends BaseDrawerFragment{

    @ViewById(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    //region life cycle

    @Override
    protected void main(boolean isRecreated) {
        super.main(isRecreated);

        if (!isRecreated){
            //Add your initial logic here
        }
    }

    @Override
    public boolean onBackPress() {

        if (getFragmentCount() == 1){
            new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.dialog_exit_title)
                    .setMessage(R.string.dialog_exit_message)
                    .setNegativeButton(R.string.dialog_exit_negative, null)
                    .setPositiveButton(R.string.dialog_exit_positive, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getActivity().finish();
                        }
                    })
                    .show();
            return true;
        }

        return super.onBackPress();
    }

    //endregion

    //region methods

    //region ui listeners

    @Click(R.id.btMenuItem1)
    public void clickMusic(){
        closeDrawer();
    }

    @Click(R.id.btMenuItem2)
    public void clickMovies(){
        closeDrawer();
    }

    //endregion

    //region super override

    @Override
    public DrawerLayout getDrawer() {
        return drawerLayout;
    }

    @Override
    public int getActionBarTitleResId() {
        return 0;
    }

    @Override
    public int getActionBarMenuResId() {
        return 0;
    }

    @Override
    public int getFragmentContainerResId() {
        return R.id.fragment_container;
    }

    //endregion

    //endregion

}
