package com.jaguarlabs.baseproject.fragments.bases;

/**
 * Created by Carlos Briseño on 23/02/2016.
 *
 * Version 1.1 modified by Carlos Briseño on 05/05/2016
 *
 */
public abstract class BaseSinglePageFragment extends BaseFragment {

    public abstract int getPageTitleResId();
    private boolean isThisPageSelected = false;

    //region methods

    public int[] getTabResourceIcon(){return null;}

    public void onPageSelected(){
        isThisPageSelected = true;
    }

    public void onPageLeft(){
        isThisPageSelected = false;
    }

    public boolean isThisPageSelected() {
        return isThisPageSelected;
    }

    //endregion
}