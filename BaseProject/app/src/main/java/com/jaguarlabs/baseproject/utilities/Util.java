package com.jaguarlabs.baseproject.utilities;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.jaguarlabs.baseproject.BuildConfig;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.UUID;

/**
 * Created by Carlos Briseño on 01/03/2016.
 *
 * Version 1.1 modified by Carlos Briseño on 05/05/2016
 *
 */
public final class Util {

    private Util(){}

    public static class System{
        public static java.util.Date getDateFromDatePicker(DatePicker datePicker){
            int day = datePicker.getDayOfMonth();
            int month = datePicker.getMonth();
            int year =  datePicker.getYear();

            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, day);

            return calendar.getTime();
        }
    }

    /**
     * Exposes useful methods related to the device itself
     */
    public static final class Device{

        private Device(){}

        public static int dpToPx(Context context, int dp){
            float scale = context.getResources().getDisplayMetrics().density;
            return  (int) (dp*scale + 0.5f);
        }

        public static float pxTpSp(Context context, float px) {
            float scaledDensity = context.getResources().getDisplayMetrics().scaledDensity;
            return px/scaledDensity;
        }

        public static float pxToDp(final Context context, final float px) {
            return px / context.getResources().getDisplayMetrics().density;
        }

        public static float spToPx(AppCompatActivity appCompatActivity, int sp){
            DisplayMetrics dm = new DisplayMetrics();
            appCompatActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
            return sp * dm.scaledDensity;
        }

        public static boolean hasConnection(Context context) {
            boolean connected = false;
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo[] networks = connectivityManager.getAllNetworkInfo();

            for (int i = 0; i < 2; i++) {

                if (networks[i].getState() == NetworkInfo.State.CONNECTED) {
                    connected = true;
                }
            }
            return connected;
        }

        public static boolean hasCallFeature(Context context){
            if (((TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE)).getPhoneType() == TelephonyManager.PHONE_TYPE_NONE){
                return false;
            }

            return true;
        }

        public static String getUUID() {
            // If all else fails, if the user does have lower than API 9 (lower
            // than Gingerbread), has reset their phone or 'Secure.ANDROID_ID'
            // returns 'null', then simply the ID returned will be solely based
            // off their Android device information. This is where the collisions
            // can happen.
            // Thanks http://www.pocketmagic.net/?p=1662!
            // Try not to use DISPLAY, HOST or ID - these items could change.
            // If there are collisions, there will be overlapping data
            String m_szDevIDShort = "35" + (Build.BOARD.length() % 10) + (Build.BRAND.length() % 10) + (Build.CPU_ABI.length() % 10) + (Build.DEVICE.length() % 10) + (Build.MANUFACTURER.length() % 10) + (Build.MODEL.length() % 10) + (Build.PRODUCT.length() % 10);

            // Thanks to @Roman SL!
            // http://stackoverflow.com/a/4789483/950427
            // Only devices with API >= 9 have android.os.Build.SERIAL
            // http://developer.android.com/reference/android/os/Build.html#SERIAL
            // If a user upgrades software or roots their phone, there will be a duplicate entry
            String serial = null;
            try {
                serial = Build.class.getField("SERIAL").get(null).toString();

                // Go ahead and return the serial for api => 9
                return new UUID(m_szDevIDShort.hashCode(), serial.hashCode()).toString();
            } catch (Exception exception) {
                // String needs to be initialized
                serial = "serial"; // some value
            }

            // Thanks @Joe!
            // http://stackoverflow.com/a/2853253/950427
            // Finally, combine the values we have found by using the UUID class to create a unique identifier
            return new UUID(m_szDevIDShort.hashCode(), serial.hashCode()).toString();
        }
    }

    /**
     * Exposes useful methods related to log operations
     */
    public static final class LOG{

        private LOG(){}

        private static final boolean LOG = true;
        private static final String APP_TAG = "BaseProject";

        public static void i(String message){
            if (!LOG){
                return;
            }
            else{
                i(APP_TAG, message);
            }
        }


        public static void i (String tag, String message) {
            if (!LOG){
                return;
            }
            else{
                Log.i(tag, message);
            }
        }
        public static void e(String message){
            if (!LOG){
                return;
            }
            else{
                e(APP_TAG, message);
            }
        }


        public static void e (String tag, String message) {
            if (!LOG){
                return;
            }
            else{
                Log.e(tag, message);
            }
        }
        public static void w(String message){
            if (!LOG){
                return;
            }
            else{
                w(APP_TAG, message);
            }
        }


        public static void w (String tag, String message) {
            if (!LOG){
                return;
            }
            else{
                Log.w(tag, message);
            }
        }
        public static void d(String message){
            if (!LOG){
                return;
            }
            else{
                d(APP_TAG, message);
            }
        }


        public static void d (String tag, String message) {
            if (!LOG){
                return;
            }
            else{
                Log.d(tag, message);
            }
        }
    }

    /**
     * Exposes common methods related to the user interface
     */
    public static final class UI{

        private UI(){}

        //Enable this block when you use universal image loader
//        public static DisplayImageOptions displayImageOptions = new DisplayImageOptions.Builder()
//                //Configure your placeholders
////                .showImageOnLoading(R.drawable.placeholder)
////                .showImageForEmptyUri(R.drawable.placeholder)
////                .showImageOnFail(R.drawable.placeholder)
//                .cacheInMemory(true)
//                .cacheOnDisk(true)
//                .build();
//
//        public static ImageLoader imageLoader = ImageLoader.getInstance();
//
//        public static void displayImage(String imageUrl, ImageView imageView){
//            imageLoader.displayImage(imageUrl, imageView, displayImageOptions);
//        }
//
//        public static void displayImage(String imageUrl, ImageView imageView, DisplayImageOptions options){
//            imageLoader.displayImage(imageUrl, imageView, options);
//        }
//
//        public static void displayImage(String imageUrl, ImageView imageView, ImageLoadingListener imageLoadingListener){
//            imageLoader.displayImage(imageUrl, imageView, displayImageOptions, imageLoadingListener);
//        }

        public static void showToast(Context context, String message){
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }

        public static void showToast(Context context, @StringRes int stringResId){
            Toast.makeText(context, stringResId, Toast.LENGTH_SHORT).show();
        }


        public static void showError(Context context, int resId, ViewGroup viewParent){
            showMessage(context, viewParent, resId, Color.parseColor("#F50A31"), Color.WHITE);
        }

        public static void showErrorIndefinite(Context context, int resId, ViewGroup viewGroup){
            showMessageIndefinite(context, viewGroup, resId, Color.parseColor("#F50A31"), Color.WHITE);
        }

        public static void showAlert(Context context, int resId, ViewGroup viewParent){
            showMessage(context, viewParent, resId, Color.parseColor("#C2BC06"), Color.WHITE);
        }

        public static void showSuccess(Context context, int resId, ViewGroup viewParent){
            showMessage(context, viewParent, resId, Color.parseColor("#10871A"), Color.WHITE);
        }

        public static void showSuccessIndefinite(Context context, int resId, ViewGroup viewGroup){
            showMessageIndefinite(context, viewGroup, resId, Color.parseColor("#10871A"), Color.WHITE);
        }

        public static void showMessage(Context context, ViewGroup viewParent, int resId, int color, int textColor){
            if (viewParent == null){
                SnackbarManager.show(
                        Snackbar.with(context).duration(Snackbar.SnackbarDuration.LENGTH_SHORT).textColor(textColor)
                                .text(resId).color(color).swipeToDismiss(false).position(Snackbar.SnackbarPosition.TOP));
            }
            else{
                SnackbarManager.show(
                        Snackbar.with(context).duration(Snackbar.SnackbarDuration.LENGTH_SHORT).textColor(textColor)
                                .text(resId).color(color).swipeToDismiss(false).position(Snackbar.SnackbarPosition.TOP),viewParent, true);
            }
        }

        public static void showMessageIndefinite(Context context, ViewGroup viewParent, int resId, int color, int textColor){
            if (viewParent == null){
                SnackbarManager.show(
                        Snackbar.with(context).duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE).textColor(textColor)
                                .text(resId).color(color).swipeToDismiss(false).position(Snackbar.SnackbarPosition.TOP));
            }
            else{
                SnackbarManager.show(
                        Snackbar.with(context).duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE).textColor(textColor)
                                .text(resId).color(color).swipeToDismiss(false).position(Snackbar.SnackbarPosition.TOP), viewParent, true);
            }
        }

        public static void dismissCurrentMessage(){
            if (SnackbarManager.getCurrentSnackbar() != null){
                SnackbarManager.getCurrentSnackbar().dismiss();
            }
        }

        public static void setClickForCall(Context context, View view, String phone){
            if (Device.hasCallFeature(context)){
                view.setOnClickListener(new View.OnClickListener() {
                    String phone;

                    @Override
                    public void onClick(View v) {
                        Action.callNumber(v.getContext(), phone);
                    }

                    public View.OnClickListener setData(String phone){
                        this.phone = phone;
                        return this;
                    }

                }.setData(phone));
            }
            else{
                view.setOnClickListener(null);
            }
        }

        public static void setClickForEmail(View view, String email){
            view.setOnClickListener(new View.OnClickListener() {
                String email;

                @Override
                public void onClick(View v) {
                    Action.sendEmail(v.getContext(), email);
                }

                public View.OnClickListener setData(String email){
                    this.email = email;
                    return this;
                }

            }.setData(email));
        }

        public static void setClickForUrl(View view, String url){
            view.setOnClickListener(new View.OnClickListener() {

                private Context context;
                private String url;

                @Override
                public void onClick(View v) {
                    Action.openAddress(context, url);
                }

                public View.OnClickListener setData(Context context, String url){
                    this.url = url;
                    this.context = context;
                    return this;
                }
            }.setData(view.getContext(), url));
        }

        public static void setClickForAddress(Context context, View view, String address){
            view.setOnClickListener(new View.OnClickListener() {

                Context context;
                String address;

                @Override
                public void onClick(View v) {
                    Action.openAddress(context, address);
                }

                public View.OnClickListener setData(Context context, String address) {
                    address.replace(" ", "+");
                    this.context = context;
                    this.address = address;
                    return this;
                }

            }.setData(context, address));
        }

        public static void hideKeyboard(Activity activity) {
            if (activity != null && activity.getCurrentFocus() != null){
                InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
            }
        }

        public static int dpToPixels(Activity activity, int dp){
            DisplayMetrics metrics = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            float logicalDensity = metrics.density;

            return (int) Math.ceil(dp * logicalDensity);
        }

        public static boolean isResourceId(Context context, int id){
            try{
                context.getResources().getResourceEntryName(id);
                return true;
            }
            catch(Resources.NotFoundException ex){
                return false;
            }
        }

        public static void hideView(View view){
            view.setVisibility(View.GONE);
        }

        public static void invisibleView(View view){
            view.setVisibility(View.INVISIBLE);
        }

        public static void showView(View view){
            view.setVisibility(View.VISIBLE);
        }

        public static void showAnimated(final View view){
            ObjectAnimator animator = ObjectAnimator.ofFloat(view, "alpha", 0, 1);
            animator.setDuration(500);
            animator.addListener(new Animator.AnimatorListener() {

                @Override public void onAnimationStart(Animator animation) {
                    view.setVisibility(View.VISIBLE);
                }

                @Override public void onAnimationRepeat(Animator animation) { }

                @Override public void onAnimationEnd(Animator animation) {
                }

                @Override public void onAnimationCancel(Animator animation) { }
            });
            animator.start();
        }

        public static void showAnimated(final View view, int timeInMilis, final Animator.AnimatorListener listener){
            ObjectAnimator animator = ObjectAnimator.ofFloat(view, "alpha", 0, 1);
            animator.setDuration(timeInMilis);
            animator.addListener(new Animator.AnimatorListener() {

                @Override public void onAnimationStart(Animator animation) {
                    if (listener != null){
                        listener.onAnimationStart(animation);
                    }
                    view.setVisibility(View.VISIBLE);
                }

                @Override public void onAnimationRepeat(Animator animation) {
                }

                @Override public void onAnimationEnd(Animator animation) {
                    if (listener != null){
                        listener.onAnimationEnd(animation);
                    }
                }

                @Override public void onAnimationCancel(Animator animation) {
                    if (listener != null){
                        listener.onAnimationCancel(animation);
                    }
                }
            });
            animator.start();
        }

        public static void hideAnimated(final View view, int timeInMilis, final Animator.AnimatorListener listener){
            ObjectAnimator animator = ObjectAnimator.ofFloat(view, "alpha", 1, 0);
            animator.setDuration(timeInMilis);
            animator.addListener(new Animator.AnimatorListener() {

                @Override public void onAnimationStart(Animator animation) {
                    if (listener != null){
                        listener.onAnimationStart(animation);
                    }
                    view.setVisibility(View.VISIBLE);
                }

                @Override public void onAnimationRepeat(Animator animation) {
                }

                @Override public void onAnimationEnd(Animator animation) {
                    if (listener != null){
                        listener.onAnimationEnd(animation);
                    }
                    view.setVisibility(View.INVISIBLE);
                    view.setAlpha(1);
                }

                @Override public void onAnimationCancel(Animator animation) {
                    if (listener != null){
                        listener.onAnimationCancel(animation);
                    }
                }
            });
            animator.start();
        }

        public static void hideAnimated(final View view){
            ObjectAnimator animator = ObjectAnimator.ofFloat(view, "alpha", 1, 0);
            animator.setDuration(250);
            animator.addListener(new Animator.AnimatorListener() {

                @Override public void onAnimationStart(Animator animation) {
                    view.setVisibility(View.VISIBLE);
                }

                @Override public void onAnimationRepeat(Animator animation) { }

                @Override public void onAnimationEnd(Animator animation) {
                    view.setVisibility(View.INVISIBLE);
                    view.setAlpha(1);
                }

                @Override public void onAnimationCancel(Animator animation) { }
            });
            animator.start();
        }


    }

    /**
     * Exposes methods which are related to common actions a click action could trigger
     */
    public static final class Action{

        private Action(){}

        public static void sendEmail(Context context, String email){
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", email, null));
            context.startActivity(Intent.createChooser(emailIntent, "Send email..."));
        }

        public static void callNumber(Context context, String phone){
            if (Device.hasCallFeature(context)){
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone));
                context.startActivity(intent);
            }
        }


        public static void openUrl(Context context, String url){
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));
            if (intent.resolveActivity(context.getPackageManager()) != null) {
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        }


        public static void openAddress(Context context, String address){
            Intent intent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("geo:0,0?q="+address));
            if (intent.resolveActivity(context.getPackageManager()) != null) {
                context.startActivity(intent);
            }
        }


        public static void addContact(Context context, String name, String phone){
            Intent intent = new Intent(Intent.ACTION_INSERT);
            intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
            intent.putExtra(ContactsContract.Intents.Insert.NAME, name);
            intent.putExtra(ContactsContract.Intents.Insert.PHONE, phone);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            context.startActivity(intent);
        }

        public static void sendSms(Context context, String phone){
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.addCategory(Intent.CATEGORY_DEFAULT);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setType("vnd.android-dir/mms-sms");
            intent.putExtra("address", phone);
//		intent.putExtra("sms_body","Body of Message");
            if (intent.resolveActivity(context.getPackageManager()) != null) {
                context.startActivity(intent);
            }
        }
    }

    public static abstract class StringU{

        public static boolean hasValue(String string){
            return string != null && !string.isEmpty();
        }

        public static boolean hasValue(int value){
            return value >= 0;
        }

        public static boolean isEmailFormat(String email)
        {
            return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }

        public static String getMD5String(String plaintext){
            String hashtext = "";
            MessageDigest m;
            try {
                m = MessageDigest.getInstance("MD5");
                m.reset();
                m.update(plaintext.getBytes());
                byte[] digest = m.digest();
                BigInteger bigInt = new BigInteger(1,digest);
                hashtext = bigInt.toString(16);
                // Now we need to zero pad it if you actually want the full 32 chars.
                while(hashtext.length() < 32 ){
                    hashtext = "0"+hashtext;
                }
            } catch (NoSuchAlgorithmException e) {
                Util.LOG.e("Error when trying to get md5 string");
                e.printStackTrace();
            }
            return hashtext;
        }

        public static boolean isEqual(String s1, String s2){
            return s1.equalsIgnoreCase(s2);
        }

        public static boolean isEqual(String s1, EditText e1){
            return s1.equalsIgnoreCase(e1.getText().toString());
        }

        public static boolean isEqual(EditText e1, EditText e2){
            return e1.getText().toString().equalsIgnoreCase(e2.getText().toString());
        }

        public static boolean isEmpty(EditText editText){
            return editText.length() == 0;
        }

        public static boolean isEmpty(String string){
            return string.equals("");
        }

        public static String addCommaToNumber(String value){
            String proccessed = "";
            String result = "";
            int j = 0;
            for(int i = value.length(); i > 0; i--){
                j++;
                proccessed+= value.charAt(i-1);
                if (j % 3 == 0 && (i-1) != 0){
                    j = 0;
                    proccessed+= ",";
                }
            }

            for(int i = proccessed.length(); i > 0; i--){
                result+= proccessed.charAt(i-1);
            }

            return result;
        }
    }

}