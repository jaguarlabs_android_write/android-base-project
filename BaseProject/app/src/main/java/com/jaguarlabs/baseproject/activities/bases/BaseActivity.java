package com.jaguarlabs.baseproject.activities.bases;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.jaguarlabs.baseproject.components.ChildHelper;
import com.jaguarlabs.baseproject.fragments.bases.BaseFragment;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.InstanceState;
import org.greenrobot.eventbus.EventBus;

/**
 * Created by Carlos Briseño on 27/01/2016.
 *
 * Version 1.1 modified by Carlos Briseño on 05/05/2016
 * Version 1.0
 *
 */
@EActivity
public abstract class BaseActivity extends AppCompatActivity implements ChildHelper.Contract{

    private ChildHelper             childHelper;
    private boolean                 manageEventRegister             = false;
    @InstanceState
    public boolean                  isRecreated                     ;//Do not initialize this variable, it will be initialized when a onRestoreInstanceState happens


    //region life cycle

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (childHelper != null) {
            outState.putBundle("child", childHelper.onSaveInstanceState());
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if (childHelper != null) {
            childHelper.onRestoreInstanceState(savedInstanceState.getBundle("child"));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        if (getFragmentContainerResId() != ChildHelper.FRAGMENT_CONTAINER_NO_SPECIFIED){
//            childHelper = new ChildHelper(this);
//        }
        childHelper = new ChildHelper(this);

    }

    @AfterViews
    public void baseMain(){
        main(isRecreated);

        if (!isRecreated) {
            isRecreated = true;
        }
    }

    protected void main(boolean isRecreated){

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (childHelper != null) {
            childHelper.onDestroy();
        }
        if (manageEventRegister && EventBus.getDefault().isRegistered(this)){
            unregisterForEvents();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        if (childHelper != null) {
            return childHelper.onCreateOptionsMenu(menu);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (childHelper != null){
            return childHelper.onOptionsItemSelected(item);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        boolean backConsumed = false;

        if (childHelper != null){
            if (childHelper.getCount() > 0){
                backConsumed = childHelper.getCurrent().onBackPress();

                if (!backConsumed){
                    backConsumed = onLastChanceToHandleBackPress();

                    if (!backConsumed) {
                        childHelper.removeLastChild();
                        backConsumed = true;
                    }
                }
            }
        }

        if (!backConsumed){
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (childHelper != null && childHelper.getCurrent() != null){
            childHelper.getCurrent().onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (getCurrentFragment() != null){
            getCurrentFragment().onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    protected boolean onLastChanceToHandleBackPress(){
        return false;
    }

    //endregion

    //region event bus wrapper

    protected void setHandleEventRegistering(boolean manageEventRegister){
        this.manageEventRegister = manageEventRegister;
        if (manageEventRegister && !EventBus.getDefault().isRegistered(this)){
            registerForEvents();
        }
    }

    /**
     * Sends an object event at application level
     * @param event The object event to be sent
     */
    protected void sendEvent(Object event){
        EventBus.getDefault().post(event);
    }

    protected void registerForEvents(){
        if (!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }
    }

    protected void unregisterForEvents(){
        if (EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);
        }
    }

    //endregion

    //region child helper wrapper

    public String addFragment(BaseFragment baseFragment){
        return getChildHelper().addChildFragment(baseFragment);
    }

    public String addConstantFragment(BaseFragment baseFragment){
        return getChildHelper().addChildFragmentConstant(baseFragment);
    }

    public String addConstantFragment(BaseFragment baseFragment, boolean clearStack){
        return getChildHelper().addChildFragmentConstant(baseFragment, clearStack);
    }

    public BaseFragment getSpecificFragment(String tag){
        return getChildHelper().getFragment(tag);
    }

    public BaseFragment getCurrentFragment(){
        return getChildHelper().getCurrent();
    }

    public boolean removeSpecificFragment(String tag){
        return getChildHelper().removeSpecificChild(tag, false);
    }

    public boolean removeLastFragment(){
        return getChildHelper().removeLastChild();
    }

    public void removeAllFragments(){
        getChildHelper().removeAllChildren();
    }

    public void showConstantFragment(String tag, boolean clearStack){
        getChildHelper().showConstantChild(tag, clearStack);
    }

    public boolean isFragmentInList(String tag){
        return getChildHelper().isInList(tag);
    }

    public int getFragmentCount(){
        return getChildHelper().getCount();
    }

    //endregion

    //region interface implementations

    //region ChildHelper.Contract

    @Override
    public AppCompatActivity getContractActivity() {
        return this;
    }

    @Override
    public FragmentManager getContractFragmentManager() {
        return getSupportFragmentManager();
    }

    @Override
    public ActionBar getContractActionBar() {
        return getSupportActionBar();
    }

    @Override
    public ChildHelper getChildHelper() {
        return childHelper;
    }

    @Override
    public int getFragmentContainerResId() {
        return 0;
    }

    @Override
    public View getRootView() {
        View view = findViewById(android.R.id.content);

        if (view != null && view instanceof ViewGroup){
            return ((ViewGroup)view).getChildAt(0);
        }

        return null;
    }

    //endregion

    //endregion
}
