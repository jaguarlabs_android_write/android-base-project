package com.jaguarlabs.baseproject.fragments.bases;

import android.support.v4.view.ViewPager;
import android.view.MenuItem;

import com.astuetz.PagerSlidingTabStrip;
import com.jaguarlabs.baseproject.R;
import com.jaguarlabs.baseproject.adapters.FragmentAdapter;
import com.jaguarlabs.baseproject.components.ChildHelper;
import com.jaguarlabs.baseproject.utilities.Util;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;

import java.util.List;

/**
 * Created by Carlos Briseño on 23/02/2016.
 *
 *
 * Version 1.1 modified by Carlos Briseño on 05/05/2016
 */
@EFragment
public abstract class BasePagerFragment extends BaseFragment implements ChildHelper.CustomHelper{

    private BaseFragment currentFragment;
    private FragmentAdapter adapter;

    //region Component life cycle

    @AfterViews
    public void mainEntry(){
        final List<BaseSinglePageFragment> pageFragments = getPageFragments();

        if (pageFragments == null || pageFragments.size() == 0){
            throw new RuntimeException("The list of fragments for BasePagerFragment must not be null nor 0 size");
        }

        ViewPager viewPager = getViewPager();
        PagerSlidingTabStrip tabs = getTabs();

        pageFragments.get(0).onPageSelected();
        adapter = new FragmentAdapter(getApplicationContext(), getChildFragmentManager(), pageFragments);

        if (viewPager != null){
            viewPager.setAdapter(adapter);

            viewPager.postDelayed(new Runnable() {
                @Override
                public void run() {
                    showPageTitleAndMenu(0);
                }
            }, 250);

            if (tabs != null){
                tabs.setViewPager(viewPager);
            }

            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    showPageTitleAndMenu(position);

                    for(int i = 0; i < pageFragments.size(); i++){
                        BaseSinglePageFragment singlePageFragment = pageFragments.get(i);

                        if (i == position){
                            singlePageFragment.onPageSelected();
                        }else{
                            singlePageFragment.onPageLeft();
                        }
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
        }
    }

    //endregion

    //region Methods

    //region Private

    private void showPageTitleAndMenu(int position){
        currentFragment = adapter.getFragmentAtPosition(position);
        getActivity().invalidateOptionsMenu();

        if (currentFragment.getChildHelper() != null){
            currentFragment.getChildHelper().setUpActionBar();
        }
    }

    //endregion

    //region Super override

    @Override
    public String getName() {
        return "BasePagerFragment";
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean result = false;

        if (currentFragment != null){
            result = currentFragment.onOptionsItemSelected(item);
        }

        return result;
    }

    @Override
    public boolean onBackPress() {
        boolean result = false;

        if (currentFragment != null){
            result = currentFragment.onBackPress();
        }

        return result;
    }

    @Override
    public int getActionBarTitleResId() {
        return currentFragment != null ? currentFragment.getActionBarTitleResId():0;
    }

    @Override
    public int getActionBarMenuResId() {
        return currentFragment != null ? currentFragment.getActionBarMenuResId():0;
    }

    @Override
    public int getFragmentContainerResId() {
        return 0;
    }

    //endregion

    //region Interface implementations

    //region ChildHelper.CustomHelper

    @Override
    public ChildHelper.Contract getChildContract() {
        return currentFragment;
    }

    //endregion

    //endregion

    //endregion

    //region Inner definitions

    //region Abstract methods

    public abstract ViewPager getViewPager();
    public abstract PagerSlidingTabStrip getTabs();
    public abstract List<BaseSinglePageFragment> getPageFragments();

    //endregion

    //endregion

}
