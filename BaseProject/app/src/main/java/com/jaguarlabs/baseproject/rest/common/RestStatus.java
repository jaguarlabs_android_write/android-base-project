package com.jaguarlabs.baseproject.rest.common;

/**
 * Created by Carlos Briseño on 09/05/2016.
 */
public enum RestStatus {

    Added,
    Updated,
    Deleted,
    Failed

}
